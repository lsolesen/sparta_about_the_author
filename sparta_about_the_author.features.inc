<?php
/**
 * @file
 * sparta_about_the_author.features.inc
 */

/**
 * Implements hook_views_api().
 */
function sparta_about_the_author_views_api() {
  return array("api" => "3.0");
}
